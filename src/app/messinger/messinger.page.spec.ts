import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessingerPage } from './messinger.page';

describe('MessingerPage', () => {
  let component: MessingerPage;
  let fixture: ComponentFixture<MessingerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessingerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessingerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
