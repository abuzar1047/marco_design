import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-splashscreen',
  templateUrl: './splashscreen.page.html',
  styleUrls: ['./splashscreen.page.scss'],
})
export class SplashscreenPage implements OnInit {
  lottieConfig: object;
  anim: any;
  constructor(private nav: NavController) {
    this.lottieConfig = {
      path: 'assets/splash.json',
      renderer: 'canvas',
      autoplay: true,
      loop: true
    };
  }
  handleAnimation(anim: any) {
    this.anim = anim;
  }

  ngOnInit() {
    setTimeout(() => {
      this.nav.navigateRoot('home');
    }, 3000);
  }

}
