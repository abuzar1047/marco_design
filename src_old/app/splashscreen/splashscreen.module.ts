import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SplashscreenPage } from './splashscreen.page';
import {LottieAnimationViewModule} from 'ng-lottie';

const routes: Routes = [
  {
    path: '',
    component: SplashscreenPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LottieAnimationViewModule.forRoot()
  ],
  declarations: [SplashscreenPage]
})
export class SplashscreenPageModule {}
